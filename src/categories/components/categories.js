import React from 'react'
import Category from './category'
import './categories.css'
import SearchContainer from '../../widgets/containers/SearchContainer'
import Media from '../../playlist/components/media'

function Categories(props) {
  return(
    <div className="Categories">
      <SearchContainer />
      {
        props.isLoading &&
        <p>Buscando tu video favorito...</p>
      }
      {
        props.search.map((item) => {
          return <Media
            title  = {item.get('title')}
            author = {item.get('author')}
            type   = {item.get('type')}
            cover  = {item.get('cover')}
            src    = {item.get('src')}
            id     = {item.get('id')}
            key    = {item.get('id')}
            openModal = {props.handleOpenModal} 
          />
        })
      }
      {
        props.categories.map((item) => {
          return (
            <Category 
              description={item.get('description')}
              title={item.get('title')}
              playlist={item.get('playlist')}
              key={item.get('id')}
              handleOpenModal = {props.handleOpenModal}
            />
          )
        })
      }
    </div>
  )
}

export default Categories