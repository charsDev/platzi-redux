import React from 'react'

function Icon (props) {
  // console.log (props)
  const {color, size} = props

  return (
    <svg
      fill = {color}
      height = {size}
      width = {size}
      viewBox = "0 0 32 32"
    >
      {props.children}
    </svg>
  )
}

export default Icon

/* 
--- TODO ESTO ES props.children ---
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
<title>play3</title>
<path d="M6 4l20 12-20 12z"></path>
</svg> */