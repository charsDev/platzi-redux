import React from 'react'
import error from '../../../images/homer-doh.png'
import './regular-error.css'

function RegularError(props) {
  return (
    <div className="Error">
      <img src={error} width={400}/>
      <h1 style={{color: 'white'}}>Ha ocurrido un error</h1>
    </div>
  )
}

export default RegularError