import api from '../api.json'

import { normalize, schema } from 'normalizr'


// schema parametros
// schema.Entity(key, definicion de mi esquema, opciones)

// schema media
const media = new schema.Entity('media', {}, {
  idAttribute: 'id',
  processStrategy: (value, parent, key) => ({...value, category:parent.id })
})

// schema category
const category = new schema.Entity('categories', {
  playlist: new schema.Array(media)
})

// result categories
const categories = { categories: new schema.Array(category)}

// Normalizando la api
const normalizedData = normalize(api, categories)

export default normalizedData