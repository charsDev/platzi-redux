# Introduccion a Redux
## __Conociendo las bases de Redux__
* Creado por [@dan_abramov](https://twitter.com/dan_abramov?lang=es)
* Redux is a predectible state container for JS apps (contenedor de estados predecibles para JS app)
* Redux(state) envuelve a la UI(app) y le dice que esta pasando, que datos tiene, que le pasa, etc
* Redux = Un estado (state) general de la app
* Redux existe debiddo a que las apps son cada vez mas dinamicas
* Puntos importantes de __Redux__:
  * __Store:__ El centro y la verdad de todo, actualiza el state con metodos para actualizar, obtener y escuchar datos. Seria lo que envuelve la UI(app).
  * __Acciones:__ Las acciones son un bloque de informacion que envia datos desde la aplicacion hacia el store. Como actulizamos y cuando cambiar. Acciones disparadas por usuario y por la propia app.
  * __Reducers__ Cambian el estado(state) de la aplicacion. 
* Como funciona la app __(Ciclo de Redux)__:
  * __UI:__ Componentes de React, trigger un click
  * __Actions:__ Recibe el cambio(click) y lo envia informacion a Reducer
  * __Reducer:__ Recibe la informacion, la cambia y la manda al store
  * __Store:__ Notifica al state de los cambios
  * __State:__ Notifica a la UI para que se actualice
![](https://image.ibb.co/i24p8o/ciclo_redux.jpg)

## __Principios a tener presentes al trabajar con Redux__
* Tres pincipios:
  * __Unica fente de la verdad__ 
    * El store lo va a saber todo (cambios, actualizaciones, etc). 
    * Una single page debe tener una sola store. Si tiene mas paginas (inicio, contacto, etc) lo correcto es que tenga un store por cada pagina
  * __El estado es de solo lectura__ 
    * No podemos manipular el estado(state) a nuestro gusto. 
    * La manipulacion del estado debe ser segun como el ciclo de Redux(UI, Action, Reducer, Store, State). 
    * Solo es consultar el state actual en el que estoy.
  * __Los cambios se realizan con funciones puras__ 
    * Esto apunta directamente a los reducers que son funciones puras. 
    * __Funciones puras__: funciones no muy complejas y faciles de leer(Programacion funcional).
* Quiza una app no necestie Redux, si lo que hace la app es algo cotidiano, no hace llamadas a API's, no tiene codigo asincrono, etc. Muchas veces no es necesario utilizar Redux. La idea es no tener una app sencilla con reingeniera con Redux.
* Redux ama a React pero puedes utilizarlo con vainilla.js o con cualquier otra libreria o framework.

## __Preparando nuestro entorno de trabajo__
* Instalar el proyecto de platzi-video ya que es el mismo que usaremos para este curso.
* Instalar la dependecia Redux, esto es del proximo capitulo pero me adelante

## __Haciendo la configuración inicial de Redux__
* En los archivso de configuracion __webpack.dev.config.js__, __webpack.config.js__:
  * Agregamos la entry redux con su respectivo path
* Creamos el archivo __redux.html__:
  * Cambiamos el title
  * Agregamos los estilos
  * En el body agregamos un form con el __id form__ y dentro un input con el __name title__
* Dentro de la carpeta entries creamos el archivo redux.js:
  * Aqui vamos a usar [VainillaJS](http://vanilla-js.com/)
  * Creamos una __const $form__ que tomara el valor del elemento que tenga el id = form en el documneto
    * Declarar una variable con un __signo de $__ antes que el nobre es una convencion para indicar que el valor de esta variable viene del DOM
  * A $form le agregamos en evento __submit__ que llamara a la funcion __handleSubmit__
  * Creamos la funcion __handleSubmit__ que recibe event como argumento:
    * Prevenimos que se recargue la pagina.
    * Declaramos una __const data__ la cual tomara los datos del $form mediante la [Web API FormData](https://developer.mozilla.org/en-US/docs/Web/API/FormData/FormData). 
      * Se le antepone __new__ ya que la API es una class.
      * Pasamos como argumento el $form
    * Creamos __const title__ la cual obtendra mediante el metodo __get__ el name que sea igual al valor __title__.
    *Imprimimos en consola.
    * OJO con los events, se puede obtener de otra forma

# Utilizando Redux con Vanilla.js
## __Entendiendo y agregando un Store__
* Recordando que es un __Store__
  * El centro y la verdad de todo, con metodos para actualizar, obtener y escuchar datos
    1. Contiene el estado de la aplicacion
    2. Puedes acceder al estado con el metodo __getState()__
    3. Puedes actualizar el estado con el metodo __dispatch(action)__
    4. Escucha cambios con el metodo __subscribe(listener)__
    5. Deja de escuchar cambios retornando la funcion del metodo __subscribe(listener)__
* La manera de llamar a Store es de la siguiente forma:
  * Llamamos al metodo __createStroe__ de redux (import {createStore} from 'redux').
  * createStore tiene 3 parametros:
    * __Reducer:__ funcion qpura que retorna el proximo estado.
    * __PreloadState / InitialState:__ estado inicial de la aplicacion, primera carga, llamdo al api. Puede ser cualquier tipo de dato.
    * __Enhancer:__ funcion que puede extender redux con capacidades añadidas por librerias externas. Es un parametro opcional.
* En el archivo __redux.js__
  * Importamos el metodo __createStore__ de redux para poder crear el store inicial
  * Creamos una constante __initialState__ que tendra los titulos de algunas canciones
  * Creeamos la constante __store__ que sera igual al metodo createStroe y le pasamos los siguientes parametros:
    * __reducer__ que es igual a una funcion que recibe de parametro state y lo retorna
    * __initialStae__ que es igual a la constante que declaramos mas arriba. Gracias a ES6 si la variable lleva el mismo nombre que el contenido, solo se pone una vez.
    * __enhancer__ pasamos el codigo para poder utilizar las [redux dev-tools-extension](https://github.com/zalmoxisus/redux-devtools-extension#usage) en el navegador.

## __Imprimiendo datos del estado__
* En el archivo __redux.html:__
  * Agregamos una etiqueta con el id __playlist__ donde se renderizaran los titulos de las canciones
* En el archivo __redux.js:__
  * Declaramos una constante __$container__ que sera igual al elemento que tenga el id __'playlist__ en el documento
  * Declaramos una constante __playlist__ que contendran todos los valores de nuestro state, en este caso los valores de la lista initialState que declaramos antes.
  * A la constante __playlist__ le aplicamos un metodo __forEach__ y dentro una funcion que recibe como parametro el __item__(el valor de cada elemento de la lista):
    * Declaramos la constante __template__ que creara una etiqueta del tipo __p__ en el documento
    * Llamamos al metodo __textContent__ de la constante template y le pasamos el valor del titulo del __item__
    * En la constante __$container__ anexamos __(appendChild)__ el valor de la constante __template__, el cual es el valor del item.title dentro de una etiqueta del tipo p
  * Imprimimo en pantalla el valor del state (initialState)

## __Definiendo acciones a manejar__
* Ya tenemos el store, tiene un state y esta siendo utilizado para mostrar datos en pantalla.
* Ahora queremos añadir mas datos a nuestra app, para eso estan las __Actions(acciones)__ que son:
  * Bloque de informacion que envia datos a la aplicacion
    * Se envian usando el metodo __dispatch()__ del __Store__
    * Son la unica fuente de informacion del __Store__
    * Son objetos planos de JS
* En el archivo __redux.js__ dentro de la funcion __handleSubmit__
  * Enviamos al __store__ on objeto plano mediante el metodo __dispatch()__
  * Lo importante en los objetos planos es la key __type__ que por convencion es el nombre en mayuscula de una funcion y separada por guiones bajos
  * Para pasarle inforamcion utilizamos __payload__ el cual sera un objeto que contendra las llaves con sus valores.
    * Se propone que se pase un objeto como valor al payload, ya que en futuras actualizaciones si se requiere pasar mas valores, solo se agreagan al objeto.
    * Gracias a ES6 si el nombre del valor es igual al de la key, solo se pone una vez, en este caso title y no title: title.
* Si verificamos la conosola del navegador, en la pestaña de __redux__ veremos que al escribir un texto y hacer submit en la consola se muesta el __ADD_SONG__, el cual todavia definiermos su funcionalida, en el proximo video.

## __Actualizando el Store con Reducers__
* Los __Reducers__ modifican el estado(state)
  1. Es una funcion pura
  2. Puede haber mas de un reducer en una app pero solo un store
  3. Devuelve el siguiente estado
* __Nunca__ hacer en un __reducer__
  1. Modificar sus argumentos
  2. Realizar tareas con efectos secundarios, como llamados a una API
  3. Llamar a funciones no puras como Date.now(), Math.random()
* ¿Que es una __funcion pura__?
  * Parte de la programacion funcional
  * Codigo mas legibles
  * Para que una funcion sea __pura__ debe considerarse 2 condiciones:
    * Dado los mismos parametros/argumentos/entradas deben retornar el mismo resultado, sin importar el numero de veces que llame
    * La funcion no debe tener efectos secundarios
* En el archivo __redux.js:__
  * Creamos una constante __reducer__, la cual contendra una funcion que recibe como parametros el state y el action.
    * El __state__ seria el que se encuentra en ese momento que se llama la funcion, en este ejemplo sera initialState en el primer llamado
    * El __action__ lo enviamos con el __store.dispatch__ que es igual a un objeto que contiene las llaves type y payload
  * Utilizamos un __switch__ para cada caso de modificacion
    * El type de la accion viene dentro del __action.type__
    * En caso de que el action.type coincida con alguno de los casos __ADD_SONG__ 
    * Retorna un arreglo en el cual mediante los spread operators(...) llamamos al state actual y le agregamos el __action.payload__
    * En caso de no coincidir con ningun caso __default__ retorna el state actual
  * Dentro de la constante __store__
    * Cambianos la funcion (state) => state por el nombre de la constante __reducer__ y ya que coincide la key con el nombre de la constante que contiene la info, nada mas delcaramos reducer una vez(ES6)

## __Actualizando la aplicacion en cada cambio con Subscribe__
* En el archivo __redux.js__
  * Creamos una funcion __render__
    * Dentro de la funcion metemos el proceso que desarrollamos para __imprimir datos del estado__
    * Agreagamos una linea en la cual dejamos en blanco el html __inner.HTML__, esto con la finalidad que cuando agregamos un titulo, no vuelva a imprimir toda la lista mas la cancion dos veces, si no que refresque la lista de titulos de canciones mas la cancion nueva. El comentario es que la lista se actualiza en cada cambio y se refresca en el DOM, para pocos titulos de canciones no hay problema, pero para registro de millones de titulos, vovlera lento el navegador. Para eso usamos React y su reactividad.
    * Llamos a la funcion __render()__ para que pinte por primera vez la lista con el estate inicial
    * Creamos una funcion __handleChange__ la cual llamara al metodo render cada que sea llamada
    * Utilizamos el metodo __subscribe__ de store y le pasamos como argumento la funcion __handleChange__
    * El metodo __subscribe__ se encarga dde actulizar los cambios en el store

# Usando Redux con React.js
## __Instalando React-redux__
* Instalamos la dependecia [react-redux](https://github.com/reduxjs/react-redux)
* En el package.json quitamos los ^ a las dependecias para que no se actualicen a las versiones mas actuales

## __Puliendo la estructura de datos de Platzi Video__
* En el archivo api.json se cambiaron algunos id dentro de playlist para que no se repitieran

## __Agregando un Store a Platzi Video__
* En el componente __entries\home.js__
  * Importamos el __createStore__ de redux
  * Declaramos la constante __initialState__ para inicializar el state
    * Dentro declaramos un objeto __data__ que tendra como valores la data que importamos del archivo __api.json__(spread operator)
  * Declaramos una constante __store__ la cual sera igual a la funcion __createStore__  que recibe como parametros
    * __reducer__ en este caso es una funcion que devuelve un valor, ya que todavia no creamos la funcion que actualizara el estado
    * __initialStae__ estado inicial
    * __enhancer__ para extender funcionalidad de redux con las redux-devtools
  * Llamamos un console.log para cerificar que los datos sean los deseados

## __Integrando el Store con Provider___
* En el componente __entries\home.js__
  * Importamos __Provider__ de react-reddux
    * Provider es una high order component (componente de alto orden)
    * Recibe los atributos que le pasemos, pero ademas ya tiene por default otros (getState, subscribe, etc). Gracias a esto ya no usaremos subscribe
  * En el __render__ cambiamos la estructura de este metodo
    * Como primer parametro llamamos al componente __Provider__ y le pasamos el __store__ 
      * Dentro del componente Provider, llamamos al componente __Home__ 
    * El segundo parametro no cambia.

## __Conectando datos a los componentes__
* Una habilidad de __redux__ es que podemos pasarle un pedazo del state general a un componente
* Para conectar los datos del store a un componente en especifico utilizamos __connect__
* __connect__ va a ser una funcion, que recibe 2 parametros
  * Un parametro donde vamos a poder especificar que datos necesitamos y esos datos los obtendremos desde el state 
  * Otro parametro donde indicaremos en que componente podran ser utilizados 
* Hace una redefinición de las propiedades del componente, algo que no está permitido por defecto en React … es por ello que esta función de tipo currificada se importa de manera adicional desde ‘react-redux’
* En el componente __containers\home.js__
  * Importamos __connect__ desde react-redux
  * En el componente __Categories__ cambiamos de donde va a obtener el valor del atribtuo Categories, en lugar de esperar un this.props.data.categories, ahora lo obtendra directamente de this.props.categories, gracias a connect y react-redux.
  * Creamos una funcion __mapStateToProps__ que recibe dos parametros state, props
    * __state__ es el estado que obtuvimos en el archivo entries\home.js, en la constante initialState gracias a redux
    * __props__ son propiedades que pueden ser pasadas directamente al componente, como lo veniamos haciendo antes
  * Dentro de la funcion returnamos un objeto con la key categories que sera igual al state.data.categories que le pasamos gracias al connect
  * En el export default llamamos a __connect__ el cual recibe dos parametros, como ya se menciono, uno donde le decimos que datos recibira(state) y otro donde se pasa el nombre del componente donde se hara la conexion(Hone)

## __Agregando un Reducer para manejar los datos__
* En el archivo __entries\home.js__
  * Importamos __reducer__ desde el archivo en la ruta __reducers\data.js__
  * Moidificamos la constante __store__ ahora en lugar de recibir como parametro una funcion que devuelve el state, recibe el reducer
* Creamos el archivo __data.js__
  * Creamos la funcion __data__ que recibe como parametro el __state__ y __action__
    * Inicamos el switch pasando el __action.type__
    * Declaramos un __case__, retornamos uns tate
    * Declaramos un __default__ y retornamos un state
* Mas adelante se declararan los casos necesarios

## __Manejando una accion para busqeuda de videos__
* En el componente __SearchContainer.js__
  * Importamos __connect__ desde react-redux. Si bien este componente no requiere el state, connect dentro de las propiedades que pasa por default cuenta con la de __dispatch__
  * Llamamos a las propiedad __dispatch__ que viene por default en el __conect__ y le pasamos como parametro un objeto
    * Un __type__ el cual nos sirve para identificarlo en los casos del switch en el archivo __data.js__
    * Un objeto __payload__ y dentro un query con el valor del input
  * Modificamos el export default para poder utilizar __connect__ en este componente y sus propiedades por default
* En el archivo __data.js__
  * Cambiamos el case, ya que declaramos uno en el componente __SearchContainer.js__

## __Filtrando los datos de busqueda__
* En el componente __entries\home.js__
  * En el objeto data del initialState pasamos una key mas __search__ que sera un array vacio
* En el archivo __data.js__
  * Creamos una costante __lista__ que sera igual a un array vacio
  * Con el metodo __map__ recorremos las categorias del state
    * Por cada item que devuelva los filtramos cada uno de los valores por el author y devuelva lo que se ingrese en el input __action.payload.query__ y lo agregue al array que declaramos en la constante __lista__ 

## __Añadiendo los datos filtrados a la UI__
* En el componente __entries\home.js__
  * En la constante __initialState__ sacamos el array __search__ del objeto data
* En el componente __containers\home.js__
  * En el elemento __Categories__
    * Le pasamos la propiedad __search__ que es igual al search que viene por las props de redux
  * En la function mapStateToProps agregamos __search__ al objeto que returnamos
* En el componente __categories.js__
  * Despues del elemento __SearchContainer__:
    * Recorremos la propiedad search y cada item lo pasa como propiedad al componente __Media__
* En el archivo __data.js__
  * Agregamos una condicional __if__ ya que si el action.payload.query viene sin nada, osea que den enter al input sin escribir nada, entonces no retorne nada. Anteriormente si le damos enter al input sin nada volvia a renderear todos los elementos

# Conceptos avanzados de Redux
## __Normalizando datos__
![](https://image.ibb.co/n3aL4T/normalizardatos.jpg)
* __Datos NO normalizados__ 
  * Lo mas cercano a como esta funcionando actualmente la app (datos)
  * Basicamente la estructura de como evuelve los datos una API Rest gracias a la bd
* __Datos normalizados__
  * Lo mas parecida a un mapa de datos(objeto JS)
  * Un millon de consultas (No normalizados) vs Una consulta (Normalizados)
* Libreria __normalizr__
  * [Normalizr](https://github.com/paularmstrong/normalizr) Sirve para poder normalizar los datos
* Creamos la carpeta __schemas__ y dentro el archvio __index.js__
  * Importamos las funciones __normalize__ y __schema__ desde __normalizr__
    * __normalize__ funcion principal a la que le pasamos los datos de origen y mis esquemas(como queremos que empiece a dividir los datos)
    * __schema__ son las divisones generales de nuestros datos normalizados(categories, media en la imagen de arriba)
  *  Importamos la __API__ de donde viene la informacion, en este caso del archivo __api.json__
  * Creamos un __schema__ para __media__  que llama una clase llamada __Entity__
    * Pasamos el nombre del key que llevara este esquema en la bd, para esete caso sera __media__
    * Recibe otro parametro, un objeto __definicion de esquema__, que sirve si queremos heredar schemas dentro de otros schemas, pero como media ya no tiene mas schemas dentro, no se pasa nada
    * Un objeto como tercer parametro que seran las opciones
      * __idAttribute__ el nombre del key que sera tomado como id unico. En nuestro caso dentro de playlist ya se cuenta con el key id
      * __ProcessStrategy__ una funcion que recibe 3 parametros
        * __value__ sera el contendio de nuestro objeto media(playlist)
        * __parent__ el padre de media y su contenido
        * __key__ que sera igual al __idAtrribute__ que declaramos anterirormente
      * Dentro de la funcion retornamos un objeto que 
        * Contendra todos los valores de media(playlist)
        * Le agregamos un key __category__ que contenga el id del padre de media en este caso Categories
        * Por default 
  * Creamos un __schema__ para __category__  que llama una clase llamada __Entity__
    * Aqui usaremos la __definicion de esquema__
      * Declaramos una nueva llave __playlist__ que sera igual a un nuevo schema de __tipo array__ del schema __media__    
  * Creamos un constante __categories__
    * Dentro un objeto con una key __categories__ que contendra en nuevo schema de __tipo array__ de __category__
  * En una constante __normalizedData__ guardamos todo nuestro __schema normalizado__, esto mediante la funcion __normalize__ la cual recibe 2 argumentos
    * Primero pasamos mis datos de origen, la api
    * Segundo pasamos un schema, el cual nos devolvera los datos normalizados. En este caso sera el scheama categories ya que el contiene el esquema category que dentro utiliza media para hacer las divisiones
* En el componente __home.js__ importamos __normalizedData__
  * Imprimimos en pantalla para ver el resultado de la normalizacion
    * __entities__ son las entidades que creamos
    * __result__ el numero de categories que existen, esto viene de la constante categories del archivo __schemas\index.js__

## __Añadiendo datos normalizados al store__
* En el componente __entries\home.js__
  * Comentamos el import de data al __api.json__
  * El import __normalizedData__ cambia, ahora sera __data__
  * En el objeto __data__ del __initialState__
    * Eliminamos el spred operator __data__
    * Creamos dos nuevas keys
      * __entities__ que es igual al valor del objeto entities de la nueva data
      * __categories__ es el valor de las categories del objeto result de la nueva data
* En el componente __pages\containers\home.js__
  * En la funcion __mapStateToProps__
    * Creamos una nueva constante __categories__ que recibe un objeto categories, que es igual a un array con los ids de las categorias, lo recorremos con el metodo map y por cada id retornaremos la categoria correspondiente.

## __Usando datos normalizados en las playlist__
* En el componente __playlist.js__
  * Importamos el componente __MediaContainer__
  * Cambiamos el nombre del parametro del metodo __map__
  * Agregamos el componente __MediaContainer__
    * Agregamos el atributo __id__
    * Cambiamos el valor de __key__
  * Recordar que el props.playlist solo devuelve un array con los ids de las canciones
* Creamos el componente __mediaContainer.js__
  * Importamos componente __Media__
  * Importamos __connect__ de react-redux
  * Renderamos el componente __Media__ y le pasamos la data que obtenemos de la funcion __mapStateToProps__(nombre por convencion)
  * Creamos la funcion __mapStateToProps__ con los parametros state y props
    * Retornamos un objeto con el key __data__ que obtendra desde el state.data.entities la media segun el id que reciba por las props
  * En el export default utilizamos connect para pasarle los parametros de __mapStateToProps__ al componente

## __Añadiendo múltiples reducers__
* En el componente __entries\home.js__
  * Importamos __reducer__
  * Comentamos el __initialState__ ya que se pasara directamente a los reducers
  * Declaramos un objeto vacio en la constante __store__ en lugar de reducer
* En el componente __pages\containers\home.js__
  * En la funcion __mapStateToProps__ en donde retornamos el key search, cambiamos la ruta, ya que este state.data.search viene ahora del reducer index.js, el cual hace una combinacion de los dos reducers y devuelve como data lo que este dentro del reducer data y como modal lo que este dentro del reducer modal
* En el archivo __data.js__
  * Importamos el archivo __schema__
  * Creamos la constante __initialState__ el cual contrendra los datos para este reducer, las entities, categories, search
  * Modificamos el parametro __state__ de la __funcion data__ y ahora recibira un valor inicial que sera igual al __initialState__
* Creamos el archivo __reducer\index.js__
  * Importamos __data__, __modal__
  * Importamos __combineReducers__ el cual combinara los reducers data y modal
  * Creamos la constante __rootReducer__ que sera igual a la combinacion de los reducers data y modal
* Creamos el archivo __modal.js__
  * Creamos la constante __initialState__ con los datos para este reducer, visibility y mediaId
  * Creamos la funcion __modal__ la cual recibie como parametro un state que sera igual al __initialState__
    * Declaramos un switch con el action.type
    * Los case sus returns
    * Un default

## __Usando datos inmutables en Platzi Video__
* Instalamos [redux-inmutable](https://github.com/gajus/redux-immutable), [Inmutable-JS](https://facebook.github.io/immutable-js/) nos sirven para tener un estado inmutable. 
* Inmutable-JS 
  * Nos provee de metodos para hacer busquedas y hacer modificaciones, hacer nuevos estados de nuestro estado previo.
  * Tiene meteodos para convertir cualquier cosa dentro de JS (arrays, object,etc) y volverlo un dato inmutable.
    * Cualquier objeto de JS se volveria un mapa
    * Un array se volveria una lista
* En los reducers __data.js__ y __modal.js__
  * Importamos __fromJS__
  * Modificamos el __initialState__ ahora el objeto, sera el argumento del metodo __fromJS__
* En el reducer general __index.js__
  * Importamos __combineReducers__ pero ahora de redux-immutable
    * Este metodo nos devuelve los reduceres data y modal combinados e inmutables
* Debido a que los datos dejaron de ser objetos ahora son mapas inmutables, debemos acceder a ellos de una manera diferente
  * En el componente __containers\home.js__
    * En la funcion __mapStateProps__ modificamos la forma de obtener los datos de la constante __categories__, agregamos el metodo get(datos inmutables)
* En el componente __entries\home.js__
  * En la constante __store__
    * Debemos volver inmutable el state inicial. En este caso como es un objeto vacio, solo se pasa un __map__ sin argumentos
* En el componente __categories.js__
  * Donde mandamos a llamar al componente __Media__, __Category__ le pasamos las props, cambiamos la manera de llamar al key, ya que ahora es un mapa
* En el comoponente __mediaContainer.js__
  * Cuando returnamos el componente __Media__ agregamos el metodo __.toJS()__ el cual convierte en un objeto al mapa del cual hacemos referencia
  * En la funcion __mapStateToProps__ cambiamos la estructura de la __data__

## __Actualizando un estado inmutable__
* En el archivo __data.js__
  * Cambiamos para que __search__ no sea un array si no un string
  * Debido a que ahora tenemos estados immutables cambiamos el if que teniamos y lo renplasamos con un __state.set__, que es una funcion que sirve para cambiar los valores de los mapas inmutables. Recibe dos parametros:
    * El nombre del mapa a cambiar __serarch__
    * Y el __action.payload.query__ el cual declaramos en el componente __SearchContainer.js__ y lo mandamos con el dispatch
* En el componente __containers\home.js__
  * Importamos __list__
  * Creamos la constante __search__ que obtendra los datos el map search. Usamos get por que son mapas
  * Creamos la variable __searchResults__ que sera igual a una lista vacia, esto gracias a que importamos __list__ que es un metodo de __immutable__
  * Creamos una condicional __if__, si la variable __search__ tiene valor(igual a true)
    * Creamos constante __mediaList__ que obtendra todos los valores del mapa __media__
    * Filtramos la varible __mediaList__ para que por cada valor que contenga, obtenga su author y mediante el metodo __includes__ verifique si es igual al calor de la constante __seach__ y lo retorne, todo se almacena en la variable __searchResults__ 
  * Cambiamos el valor de __search__ que ahora sera igual al __searchResults__

  ## __Añadiendo las acciones y funcionalidad al modal__
  * En el componente __containers\mediaContainer.js__
    * Creamos la funcion __openModal__ que recibe un paramtero id
      * La cual hara un __disaptch__
      * Si es del type __OPEN_MODAL__
        * Tendra el objeto __payload.mediaId__, que es igual a la id que recibe como parametro
    * Le pasamos al componente __Media__ el openModal
  * En el archivo __modal.js__
    * Modificamos la funcion __modal__ para que si el case es __OPEN_MODAL__
      * Se hace un __merge__ de visibility, mediaId
        * Merge: se utiliza para setear dos o mas states
  * En el componente __containers\home.js__
    * Eliminamos el objeto state, ya que con redux tenemos una sola funte de la verdad y ya no es necesario estar pasaando ni seteando states
    * Modificamos la funcion __handleOpenModal__
      * Cambia el parametro que recibe ahora es un id
      * En lugar de setState ahora utilizamos dispatch
    * Modificamos la funcion __handleCloseModal__ para que pueda realizar un dispatch
    * Modificamos la condicion, ahora pregunta si existe la propiedad __visibility__ del mapa modal
    * En el componente __videoPlayer__
      * Cambiamos la forma de pasar las props, en lugar de mandarla una por una (src, title), le mandamos una sola id, que la obtiene del state modal
    * Agregamos __modal__ a la funcion __mapStateToProps__ para que pueda ser utilizada dentro de las props del componente
  * En el componente __video-player.js__
    * Importamos connect de react-redux
    * En el componente __Title__ cambiamos la forma de recibir la propiedad title
    * En el componente __Video__ modificamos el atributo src para que ahora tome el valor de la prop media, que es un mapa
    * Creamos la funcion __mapStateToProps__ retornamos media, para que pueda ser consumida por este componente
    * Modificamos el export default para que se conecte a la funcion mapStateToProps y pueda consumir esas propiedades en el componente
  * En el componente __categories.js__
    * En el componente __Media__ agregamos un atributo __openModal__
  * En el componente __media.js__
    * Modificamos la funcion __handleClick__ para que llame a la funcion __openModal__ y le pasa el id

## __Creadores de acciones__
* Empaquetar mi accion dentro de una funcion que reciba los parametros de esa accion y retorne la accion
* Sirve para no andar escribiendo siempre la misma accion en diferentes archivos o componentes y luego correr el riesgo de un typo
* Si la app cuenta con muchas acciones, es recomendable tener un creador de acciones para tenerlas todas en un mismo lugar
* Creamos el archivo __actions\index.js__
  * En este archivo estaran todas las acciones que utilizamos en la app. Esto para tenerlas todas en un solo lugar y asi poderlas exportar, segun se vayan requiriendo
* En el archivo __data.js__
  * En la funcion data cambiamos el nombre del case para que concuerde con el del action, esto debido a que en el archivo __actions\index.js__ declaramos el creador de accion __searchEntities__ y el type lo declaramos como __SEARCH_ENTITIES__
* En los componentes __home.js, mediaContainer.js y SearchContainer.js__
  * Importamos los creadores de acciones segun se vayan requiriendo
  * Sustituimos en el codigo las acciones por los creadores de acciones importados y se les pasa sus parametros

## __Enlazando creadores de acciones__
* Recibe un grupo de acciones y las combina con el __dispatch__ , esto para que en lugar de llamar __this.props.dispatch__ ahora seria __this.props.'accion'__
* Para esto tenemos que importar el metodo __bindActionCreators__ de redux que recibe dos parametros, las acciones y el dispatch
* En los componentes __containers\home.js, SearchContainer.js y mediaContainer.js__
  * Cambiamos el import de las actions, en lugar de traer solo los que requerimos traeremos todos con __*__ y lo renombramos a actions __as__, esto gracias a ES6
  * Importamos el metodo __bindActionCreators__ de redux
  * Creamos la funcion __mapDispatchToProps__ que recibe como parametro el dispatch y retorna un objeto con la clave __actions__ y el bindActionCreators y sus parametros __actions__ y __dispatch__
    * __actions__ son las acciones que importamos arriba (* as)
    * __dispatch__ es el dispatch que provee redux
  * Le pasamos otro parametro al __connect__ en este caso __mapDispatchToProps__

## __Action Types__
* Al igual que las acciones, podemos tener en un solo lugar los string de las acciones OPEN_MODAL, CLOSE_MODAL, SEARCH_ENTITIES
* Creamos el archivo __action-type/index.js__ en el cual declaramos las constatnes con los strings de los action types y los exportamos
* En los archivos __data.js, index.js y modal.js__
  * Importamos las constantes, segun se requieran
  * Sustitumos los valores por las nuevas constantes segun se vayan requiriendo

## __Middlewares__
* Es una forma de poder interceptar todo lo que esta ocurriendo con Redux, para mejorarlo para hacer algo extra (REDUX_DEVTOOLS_EXTENSION). Podemos hacer nuestras herramientas propias.
* Recibe un dispatch, el getState como argumnetos y regresa una funcion. Esa funcion va a recibir el metodo para despachar el siguiente middleware y se espera que devuelva una funcion que recibe action y llame next(action)
* En el componente __entries\home.js__
  * Importamos __apllyMiddleware__
  * Creamos una funcion __logger__ que recibe como parametros el __dispatch__ y el __getState__
    * Retorna una funcion con el parametro __next__ el cual sirve para avanzar a la siguente funcion
      * Retorna una funcion con el parametro __action__ y dentro hacemos todo lo que queramos, en este caso hacemos logs de un estado viejo, una busqueda y un estado despues de la busqueda. 
      * Retorna el __next(action)__, pero en este caso __value__ tiene el valor del next(action)

## __Añadiendo múltiples Middlewares__
* En el componente __entries\home.js__
  * Importamos el middleware __logger__ desde redux_logger
  * Importamos __composeWithDevTools__ desde redux-devtools-extension
  * En la constante __store__ 
    * Agregamos la funcion __composeWithDevTools__ , dentro de esta agregamos la funcion __applyMiddleware__ y le pasaremos como parametros los middlewares que vayamos a utilizar.
    * En este caso, utilizamos el middleware logger y tambien el logger_ que fue el que hicimos en el modulo pasado, de esta forma estamos usando dos middlewares.

## __Acciones asincronas__
* __redux-thunk__ Es un middleware que permite maneajr los flujos asíncronos desde dentro de las acciones.
* Instalamos la dependencia __redux-thunk__
* En el componente __SearchContainer.js__
  * En el manejador de evento __handleSubmit__ retornamos la accion __searchAsyncEntities__ con el contenido del input
* En el componente __entries\home.js__
  * Importamos __thunk__ y lo agregamos a los __middleware__ del store
* En el archivo __action_types\index.js__
  * Declaramos una constante __SEARCH_ASYNC_ENTITIES__
  *
* En el archivo de las acciones __actions\index.js__
  * Importamos el nuevo action_types __SEARCH_ASYNC_ENTITIES__
  * Creamos la funcion __searchAsyncEntities__ la cual recibe como parametro el query , retorna una funcion que recibe como parametro el dispatch y en este caso retorna un setTimeout, emulando una funcion asincrona.

## __Estados de carga__
* En el componente __components\media.js__ y los archivos de estilos playlist.css y media.css se hacen modificaciones del diseño.
* En el archivo __action_types\index.js__
  * Creamos la constante IS_LOADING con el mismo valor de tipo string
* Creamos el reducer __is-loading.js__
  * Mediante la funcion __isLoading__ que recibe como parametros el state y el action
    * En el case de que sea IS_LOADING seteara el valor del active a lo que venga por el action.payload.value
  * Exportamos el isLoading
* En el archivo __reducers\index.js__
  * Importamos el reducer __isLoading__
  * Agregamos en nuevo reducer a la constante __rootReducers__
* En el archivo __actions\index.js__
  * Importamos el action_type __IS_LOADING__
  * Creamos la funcion __isLoading__ que recibe el parametro value y retorna el type y el payload que es igual a un objeto con el valor del value
  * En la funcion __searchAsyncEntities__ ya que recibe el dispatch
    * Pasamos el dispatch con el parametro isLoading con el valor true antes del setTimeout para que muestre la frase 'buscando videos favoritos'
    * Y otro dentro del setTimeout con el valor de false de manera que cuando termine los 5 segundos se quite la frase
* En el componente __containers\home.js__
  * En el return de la funcion __mapStateToProps__ agregamos la prop isLoading que obtiene el valor del active
  * En el componente __Categories__ pasamos la prop __isLoading__
* En el componente __components\categories.js__
  * Agregamos una validacion que si la prop.isLoading es true, entonces escriba un mensaje